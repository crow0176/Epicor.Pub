﻿using System;
using System.Data;
using Ice.Adapters;
using Ice.BO;

namespace Epicor.Pub
{
    /// <summary>BAQ筛选条件</summary>
    public class BAQWhereItem
    {
        /// <summary>公司</summary>
        public string Company { get; set; }
        /// <summary>BAQ</summary>
        public string QueryID { get; set; }
        /// <summary></summary>
        public Guid SubQueryID { get; set; }
        /// <summary>表名称</summary>
        public string TableID { get; set; }
        /// <summary></summary>
        public Guid CriteriaID { get; set; }
        /// <summary>序号</summary>
        public int Seq { get; set; }
        /// <summary>表字段名称</summary>
        public string FieldName { get; set; }
        /// <summary>表字段类型(nvarchar,bit,date,decimal,int...)</summary>
        public string DataType { get; set; }
        /// <summary>操作符</summary>
        public string CompOp { get; set; }
        /// <summary>And,Or</summary>
        public string AndOr { get; set; }
        /// <summary></summary>
        public bool Neg { get; set; }
        /// <summary>左边符号(空,"(")</summary>
        public string LeftP { get; set; }
        /// <summary>右边符号(空,")")</summary>
        public string RightP { get; set; }
        /// <summary></summary>
        public bool IsConst { get; set; }
        /// <summary></summary>
        public int CriteriaType { get; set; }
        /// <summary></summary>
        public string ToTableID { get; set; }
        /// <summary></summary>
        public string ToFieldName { get; set; }
        /// <summary></summary>
        public string ToDataType { get; set; }
        /// <summary>筛选值</summary>
        public string RValue { get; set; }
        /// <summary></summary>
        public bool ExtSecurity { get; set; }
        /// <summary></summary>
        public long SysRevID { get; set; }
        /// <summary></summary>
        public Guid SysRowID { get; set; }
        /// <summary></summary>
        public int BitFlag { get; set; }
        /// <summary></summary>
        public string RowMod { get; set; }
        /// <summary>初始化函数</summary>
        public BAQWhereItem(string tableID, string fieldName, string rValue, string dataType, string andOr = "", string compOp = "=", string leftP = "", string rightP = "")
        {
            TableID = tableID;
            Seq = 0;
            FieldName = fieldName;
            DataType = dataType;
            CompOp = compOp;
            AndOr = andOr;
            Neg = false;
            LeftP = leftP;
            RightP = rightP;
            IsConst = true;
            CriteriaType = 2;
            ToTableID = "";
            ToFieldName = "";
            ToDataType = dataType;
            RValue = rValue;
            ExtSecurity = false;
            BitFlag = 0;
            RowMod = "";
        }
        // public void GetNewQueryWhereItemRow(DataRow row1)
        //{
        //    //DynamicQueryDataSet.QueryWhereItemRow 
        //    // DataRow row1 = dqa.QueryDesignData.QueryWhereItem.NewQueryWhereItemRow();
        //    //DataRow dr = dqa.QueryDesignData.QuerySubQuery.Rows[0];
        //    row1["Company"] = Company;// dr["Company"].ToString();
        //    row1["QueryID"] = QueryID;// dr["QueryID"].ToString();
        //    row1["SubQueryID"] = SubQueryID; //(Guid)dr["SubQueryID"];
        //    row1["TableID"] = TableID;
        //    row1["Seq"] = Seq;
        //    row1["FieldName"] = FieldName;
        //    row1["DataType"] = DataType;
        //    row1["CompOp"] = CompOp;
        //    row1["AndOr"] = AndOr;
        //    row1["Neg"] = Neg;
        //    row1["LeftP"] = LeftP;
        //    row1["RightP"] = RightP;
        //    row1["IsConst"] = IsConst;
        //    row1["CriteriaType"] = CriteriaType;
        //    row1["ToTableID"] = ToTableID;
        //    row1["ToFieldName"] = ToFieldName;
        //    row1["ToDataType"] = ToDataType;
        //    row1["RValue"] = RValue;
        //    row1["ExtSecurity"] = ExtSecurity;
        //    row1["BitFlag"] = BitFlag;
        //    row1["RowMod"] = RowMod;

        //}
        /// <summary>当前对象转DynamicQueryDataSet.QueryWhereItemRow</summary>
        public DynamicQueryDataSet.QueryWhereItemRow GetNewQueryWhereItemRow(DynamicQueryAdapter dqa)
        {
            DynamicQueryDataSet.QueryWhereItemRow row1 = dqa.QueryDesignData.QueryWhereItem.NewQueryWhereItemRow();
            DataRow dr = dqa.QueryDesignData.QuerySubQuery.Rows[0];
            row1.Company = dr["Company"].ToString();
            row1.QueryID = dr["QueryID"].ToString();
            row1.SubQueryID = (Guid)dr["SubQueryID"];
            row1.TableID = TableID;
            row1.Seq = Seq;
            row1.FieldName = FieldName;
            row1.DataType = DataType;
            row1.CompOp = CompOp;
            row1.AndOr = AndOr;
            row1.Neg = Neg;
            row1.LeftP = LeftP;
            row1.RightP = RightP;
            row1.IsConst = IsConst;
            row1.CriteriaType = CriteriaType;
            row1.ToTableID = ToTableID;
            row1.ToFieldName = ToFieldName;
            row1.ToDataType = ToDataType;
            row1.RValue = RValue;
            row1.ExtSecurity = ExtSecurity;
            row1.BitFlag = BitFlag;
            row1.RowMod = RowMod;
            return row1;
        }
    }
}
