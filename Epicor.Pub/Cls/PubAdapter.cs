﻿using System;
using System.Reflection;
using System.Windows.Forms;
using Ice.Lib.Framework;
using System.Collections;
using System.IO;
using System.Data;
using Ice.Lib.Searches;

namespace Epicor.Pub
{
    /// <summary>DLL反射静态对象</summary>
    public static class _PubAdapter
    {
        /// <summary>适配器哈希表</summary>
        public static Hashtable Hashtable = new Hashtable();
        /// <summary>加载DLL文件</summary>
        public static Assembly LoadAssembly(string dllfile) {
             return Assembly.LoadFrom(dllfile);
        }
    }
    /// <summary>DLL反射类</summary>
    public class PubAdapter
    {
        private Type Type = null;
        private string Name = null;
        /// <summary>DLL反射类构造函数</summary>
        public PubAdapter(EpiTransaction otrans, string name,string className1 = null)
        {
            Name = name;
            if (_PubAdapter.Hashtable[Name] == null)
            {
                string dllfile = "Erp.Adapters." + Name + ".dll";
                string className = "Erp.Adapters." + Name + "Adapter";
                if (!File.Exists(Application.StartupPath + @"\" + dllfile))
                {
                    dllfile = "Ice.Adapters." + Name + ".dll";
                    className = "Ice.Adapters." + Name + "Adapter";
                }
                if (!File.Exists(Application.StartupPath + @"\" + dllfile))
                {
                    dllfile = Name;
                    className = className1;
                }
                Assembly assembly = _PubAdapter.LoadAssembly(Application.StartupPath + @"\" + dllfile);
                Type = assembly.GetType(className);
                _PubAdapter.Hashtable[Name] = Activator.CreateInstance(Type, new object[] { otrans });
            }
        }


        /// <summary>调用funName方法,obj数组为参数,types为参数类型</summary>
        public object InvokeFun(string funName, object[] obj = null, Type[] types = null)
        {
            MethodInfo Fun;
            if (types == null) Fun = Type.GetMethod(funName);
            else Fun = Type.GetMethod(funName, types);
            return Fun.Invoke(_PubAdapter.Hashtable[Name], obj);
        }
        /// <summary>反射获取属性值</summary>
        public object GetValue(string attrName)
        {
            Type type = _PubAdapter.Hashtable[Name].GetType();
            PropertyInfo propertyInfo = type.GetProperty(attrName);
            return propertyInfo.GetValue(_PubAdapter.Hashtable[Name]);
        }
        /// <summary>反射执行BOConnect方法</summary>
        public bool BOConnect()
        {
           return (bool)InvokeFun("BOConnect");
        }
        /// <summary>反射执行Update方法</summary>
        public bool Update()
        {
            return (bool)InvokeFun("Update");
        }
        /// <summary>反射执行Dispose方法</summary>
        public void Dispose()
        {
            InvokeFun("Dispose");
            _PubAdapter.Hashtable.Remove(Name);
        }
        /// <summary>反射执行ClearData方法</summary>
        public void ClearData()
        {
            InvokeFun("ClearData");
        }
        /// <summary>反射执行GetByID方法</summary>
        public bool GetByID(object[] obj = null, Type[] types = null)
        {
            return (bool)InvokeFun("GetByID", obj, types);
        }
        /// <summary>反射执行DeleteByID方法</summary>
        public bool DeleteByID(object[] obj = null, Type[] types = null)
        {
            return (bool)InvokeFun("DeleteByID", obj, types);
        }
        /// <summary>反射执行Delete方法</summary>
        public bool Delete(DataRow dr)
        {
            return (bool)InvokeFun("Delete", new object[] { dr }, new Type[] { typeof(DataRow) });
        }
        /// <summary>反射执行GetCurrentDataSet方法</summary>
        public DataSet GetCurrentDataSet(object[] obj = null, Type[] types = null)
        {
            return (DataSet)InvokeFun("GetCurrentDataSet", new object[] { DataSetMode.RowsDataSet }, new Type[] { typeof(DataSetMode) });
        }
        /// <summary>反射执行InvokeSearch方法</summary>
        public DialogResult InvokeSearch(SearchOptions opts)
        {
            return (DialogResult)InvokeFun("InvokeSearch", new object[] { opts }, new Type[] { typeof(SearchOptions) });
        }
    }
}
