﻿using System;
using System.Data;
using Ice.Core;
using Ice.Lib.Framework;

namespace Epicor.Pub
{
    /// <summary>BOM操作类</summary>
    public class PubBOM
    {
        /// <summary>BOM列</summary>
        public PubColumn[] BOMCols = null;
        /// <summary>BOM数据表</summary>
        public DataTable BOMData =  null;
        /// <summary>数据库操作对象</summary>
        public PubDB DB = null;
        /// <summary>公司</summary>
        public string Company { get; set; }
        /// <summary>BOM操作类实例化</summary>
        public PubBOM(EpiTransaction otran)
        {
            Company = ((Session)otran.Session).CompanyID;
            DB = new PubDB(otran);
            BOMCols = PubDataModel.BOM();
            BOMData = PubDataModel.ToDataTable(BOMCols);
        }

        /// <summary>加载核准的BOM数据</summary>
        public void LoadPartBOM(string partNum,string revisionNum)
        {
            DataTable part = GetPart(partNum, revisionNum);
            if (part.Rows.Count > 0)
            {
                if (Convert.ToBoolean(part.Rows[0]["Approved"]))
                {
                    LoadBOM(part.Rows[0], 1, 1, "┗");
                }
            }
        }
       
        private void LoadBOM(DataRow mtlRow,decimal qty,int level,string symbol) {
            DataRow row = BOMData.NewRow();
            BOMData.Rows.Add(row);
            row["Symbol"] = symbol + level.ToString();
            row["Level"] = level;
            row["MtlSeq"] = mtlRow["MtlSeq"];
            row["PartNum"] = mtlRow["MtlPartNum"];
            row["RevisionNum"] = mtlRow["RevisionNum"];
            row["PartDesc"] = mtlRow["PartDesc"];
            row["Qty"] =Math.Round(qty * Convert.ToDecimal(mtlRow["QtyPer"]),8);
            row["UOMCode"] = mtlRow["UOMCode"];
            row["Type"] = "MTL";
            if (Convert.ToBoolean(mtlRow["ViewAsAsm"]) || Convert.ToBoolean(mtlRow["PullAsAsm"]))
            {
                row["Type"] = "PROD";
                DataTable partMtl = GetPartMtl(mtlRow["MtlPartNum"].ToString(), mtlRow["RevisionNum"].ToString());
                if (partMtl.Rows.Count > 0)
                {
                    for (int r = 0; r < partMtl.Rows.Count; r++)
                    {
                        string str= symbol.Replace("┗", "　").Replace("┣", "┃") + "┣" ;
                        if(r== partMtl.Rows.Count-1) str = symbol.Replace("┗", "　").Replace("┣", "┃") + "┗";
                        LoadBOM(partMtl.Rows[r], Convert.ToDecimal(row["Qty"]), level + 1,str);
                    }
                }
            }
        }

        private DataTable GetPartMtl(string partNum, string version) {
            string sqlstr = "SELECT MtlPartNum,(select PartDescription from ERP.Part WHERE Company = PartMtl.Company and PartNum = PartMtl.MtlPartNum)PartDesc, isnull((SELECT top 1 RevisionNum from ERP.PartRev with(nolock)WHERE Company = PartMtl.Company and PartNum = PartMtl.MtlPartNum and Approved = '1' and EffectiveDate <= convert(nvarchar(10), getdate(), 120) order by PartNum, EffectiveDate desc),'') RevisionNum,MtlSeq,ViewAsAsm,PullAsAsm,UOMCode,Convert(decimal(18, 8),QtyPer) QtyPer FROM ERP.PartMtl with(nolock) WHERE Company = '" + Company + "' and PartNum = '" + partNum + "' and RevisionNum = '" + version + "' and AltMethod = ''";
            return DB.ExecuteDt(sqlstr);
        }

        private DataTable GetPart(string partNum, string version)
        {
            string sqlstr = "select T1.PartNum AS MtlPartNum,T1.PartDescription AS PartDesc,T2.RevisionNum,0 AS MtlSeq, Convert(bit,1) AS ViewAsAsm, Convert(bit,1) AS PullAsAsm, IUM AS UOMCode , Convert(decimal(18, 8), 1) AS QtyPer,T2.Approved from ERP.Part T1 with(nolock) left Join ERP.PartRev T2 with(nolock) on T1.Company = t2.Company and T1.PartNum = T2.PartNum WHERE T1.Company = '" + Company + "' and T1.PartNum = '" + partNum + "' and T2.RevisionNum = '" + version + "' and T2.EffectiveDate <= convert(nvarchar(10), getdate(), 120)";
            return DB.ExecuteDt(sqlstr);
        }

    }
}
