﻿using System;
using System.Data;

namespace Epicor.Pub
{
    /// <summary>自定义列类</summary>
    public class PubColumn
    {
        /// <summary>列名</summary>
        public string ColumnName { get; set; }
        /// <summary>列标题（英文）</summary>
        public string ColumnCaptionEng { get; set; }
        /// <summary>列标题（中文）</summary>
        public string ColumnCaptionSch { get; set; }
        /// <summary>列宽</summary>
        public int ColumnWidth { get; set; }
        /// <summary>数据类型</summary>
        public Type ColumnType { get; set; }
        /// <summary>小数位长度</summary>
        public int ColumnDigits { get; set; }
        /// <summary>允许编辑</summary>
        public Boolean AllowEdit { get; set; }
        /// <summary>是否隐藏</summary>
        public Boolean Hidden { get; set; }
        /// <summary>列描述1</summary>
        public string ColumnDesc1 { get; set; }
        /// <summary>列描述2</summary>
        public string ColumnDesc2 { get; set; }
        /// <summary>列描述3</summary>
        public string ColumnDesc3 { get; set; }
        /// <summary>自定义列构造函数</summary>
        public PubColumn(string _ColumnName, string _ColumnCaptionEng = null, string _ColumnCaptionSch = null, int _ColumnWidth = 80, Type _ColumnType = null, int _ColumnDigits = 2, Boolean _AllowEdit = false, Boolean _Hidden = false, string _ColumnDesc1 = "", string _ColumnDesc2 = "", string _ColumnDesc3 = "")
        {
            ColumnName = _ColumnName;
            ColumnCaptionEng = (_ColumnCaptionEng == null ? _ColumnName : _ColumnCaptionEng);
            ColumnCaptionSch = (_ColumnCaptionSch == null ? _ColumnName : _ColumnCaptionSch);
            ColumnWidth = _ColumnWidth;
            ColumnType = _ColumnType == null ? typeof(string) : _ColumnType;
            ColumnDigits = _ColumnDigits;
            AllowEdit = _AllowEdit;
            Hidden = _Hidden;
            ColumnDesc1 = _ColumnDesc1;
            ColumnDesc2 = _ColumnDesc2;
            ColumnDesc3 = _ColumnDesc3;
        }

        /// <summary>当前对象转DataColumn</summary>
        public DataColumn ToDataColumn(string languageID = "sch")
        {
            DataColumn dc = new DataColumn();
            dc.ColumnName = ColumnName;
            dc.Caption = (languageID == "sch") ? ColumnCaptionSch : ColumnCaptionEng;
            dc.DataType = ColumnType;
            return dc;
        }
    }
}
