﻿using System;
using System.IO;
using System.Xml;

namespace Epicor.Pub
{
    /// <summary>获取配置文件类</summary>
    public class PubConfig
    {
        /// <summary>xml配置文件</summary>
        public PubXML xml = null;
        /// <summary>类实例化</summary>
        public PubConfig()
        {
            string configFile = "Epicor.Pub.config";
            if (File.Exists(configFile))
            {
                xml = new PubXML(configFile);
            }
        }


        private string GetValue(string nodePath)
        {
            XmlElement key = xml.GetNode(nodePath) as XmlElement;
            string str = key.GetAttribute("Encrypt");
            bool encrypt = false;
            if (!string.IsNullOrEmpty(str)) encrypt = Convert.ToBoolean(str);
            return encrypt ? PubFun.Md5Decrypt(key.InnerText) : key.InnerText;
        }
        /// <summary>Epicor配置文件路径</summary>
        public string Server_ConfigPath
        {
            get
            {
                return GetValue("PubConfig/Server/ConfigPath");
            }
        }//= "172.18.12.150";
        /// <summary>Epicor登录帐号</summary> 
        public string Server_UserID
        {
            get
            {
                return GetValue("PubConfig/Server/UserID");
            }
        }// = "3510167";
        /// <summary>Epicor登录密码</summary>
        public string Server_Password
        {
            get
            {
                return GetValue("PubConfig/Server/Password");
            }
        }//= "52126070";
        /// <summary>Epicor数据库IP</summary>
        public string DB_IP
        {
            get
            {
                return GetValue("PubConfig/DB/IP");
            }
        }//= "172.18.12.150"; 
        /// <summary>Epicor数据库名称</summary>
        public string DB_Name
        {
            get
            {
                return GetValue("PubConfig/DB/Name");
            }
        }//= "ReportUser";
        /// <summary>Epicor数据帐号</summary>
        public string DB_UserID
        {
            get
            {
                return GetValue("PubConfig/DB/UserID");
            }
        }//= "ReportUser";
        /// <summary>Epicor数据库密码</summary>
        public string DB_Password
        {
            get
            {
                return GetValue("PubConfig/DB/Password");
            }
        }// = "ReportUser";
        /// <summary>Epicor附件目录</summary>
        public string Attached_Path
        {
            get
            {
                return GetValue("PubConfig/Attached/Path");
            }
        }// = @"\\192.168.100.29\Edata1";
        /// <summary>邮件服务器IP</summary>
        public string SMTP_Host
        {
            get
            {
                return GetValue("PubConfig/SMTP/Host");
            }
        }// = "10.0.0.17";
        /// <summary>邮件服务器端口</summary>
        public int SMTP_Port
        {
            get
            {
                return Convert.ToInt32(GetValue("PubConfig/SMTP/Port"));
            }
        }// = 25;
        /// <summary>邮件帐号</summary>
        public string SMTP_UserName
        {
            get
            {
                return GetValue("PubConfig/SMTP/UserName");
            }
        }// = "";
        /// <summary>邮件密码</summary>
        public string SMTP_Password
        {
            get
            {
                return GetValue("PubConfig/SMTP/Password");
            }
        }// = "";
        /// <summary>SSRS报表服务</summary>
        public string SSRS_ServerUrl
        {
            get
            {
                return GetValue("PubConfig/SSRS/ServerUrl");
            }
        }// = "http://xxx/ReportServer";
    }
}

