﻿using System.Xml;
using System.Data;
using System.IO;

namespace Epicor.Pub
{
    /// <summary>XML操作类库</summary>
    public class PubXML
    {
        /// <summary>XML文件路径</summary>
        public string strXmlFile;
        /// <summary>XML文件</summary>
        public XmlDocument objXmlDoc = new XmlDocument();
        /// <summary>加载XML</summary>
        public PubXML(string XmlFile)
        {
            try
            {
                strXmlFile = XmlFile;
                objXmlDoc.Load(strXmlFile);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>获取节点属性</summary>
        public string GetNodeAttrib(string XmlPathNode, string Attrib="value")
        {
            XmlNode node = objXmlDoc.SelectSingleNode(XmlPathNode);
            XmlElement el = node as XmlElement;
            return el.GetAttribute(Attrib);
        }
        /// <summary>获取节点</summary>
        public XmlNode GetNode(string XmlPathNode)
        {
            return objXmlDoc.SelectSingleNode(XmlPathNode);
        }
        /// <summary>获取节点列表</summary>
        public XmlNodeList GetNodeList(string XmlPathNode)
        {
            return objXmlDoc.SelectNodes(XmlPathNode);
        }
        /// <summary>查找数据,返回一个DataView</summary>
        public DataTable GetData(string XmlPathNode,string tablename)
        {
            XmlNode node = GetNode(XmlPathNode);
            if (node != null)
            {
                DataSet ds = new DataSet();
                StringReader read = new StringReader(node.OuterXml);
                ds.ReadXml(read);
                return ds.Tables[tablename];
            }
            else {
                return null;
            }
        }
        /// <summary>插入一个节点</summary>
        public void InsertElement(string MainNode, string Element)
        {

            XmlNode objNode = objXmlDoc.SelectSingleNode(MainNode);
            XmlElement objElement = objXmlDoc.CreateElement(Element);
            objNode.AppendChild(objElement);
        }
        /// <summary>更新节点属性</summary>
        public void UpdateElement(string nodepath, string Attrib, string AttribContent)
        {
            XmlNode objNode = objXmlDoc.SelectSingleNode(nodepath);
            XmlElement objElement = objNode as XmlElement;
            if (!objElement.HasAttribute(Attrib))
            {
                XmlAttribute xa = objXmlDoc.CreateAttribute(Attrib);
                xa.Value = AttribContent;
                objElement.Attributes.Append(xa);
            }
            else objElement.SetAttribute(Attrib, AttribContent);
        }
        /// <summary> 更新节点内容 </summary>
        public void UpdateText(string nodepath, string Content)
        {
            objXmlDoc.SelectSingleNode(nodepath).InnerText = Content;
        }

        /// <summary>删除一个节点</summary>
        public void Delete(string Node)
        {
            string mainNode = Node.Substring(0, Node.LastIndexOf("/"));
            objXmlDoc.SelectSingleNode(mainNode).RemoveChild(objXmlDoc.SelectSingleNode(Node));
        }

        /// <summary>保存文档</summary>
        public void Save()
        {
            try
            {
                objXmlDoc.Save(strXmlFile);
                objXmlDoc = null;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
    }
}
