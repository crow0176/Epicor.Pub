﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Epicor.Pub
{
    /// <summary>WindowAPI</summary>
    public class WinAPI32
    {
        /// <summary>键盘处理事件委托 ,当捕获键盘输入时调用定义该委托的方法.</summary>
        public delegate int HookHandle(int nCode, int wParam, IntPtr lParam);         
        /// <summary>设置钩子</summary>
        [DllImport("user32.dll")]
        public static extern int SetWindowsHookEx(int idHook, HookHandle lpfn, IntPtr hInstance, int threadId);
        /// <summary>取消钩子</summary>   
        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern bool UnhookWindowsHookEx(int idHook);  
        /// <summary>调用下一个钩子</summary>  
        [DllImport("user32.dll")]
        public static extern int CallNextHookEx(int idHook, int nCode, int wParam, IntPtr lParam);        
                                                                                                                      /// <summary>获取当前线程ID</summary>    
        [DllImport("kernel32.dll")]
        public static extern int GetCurrentThreadId();
        /// <summary>Gets the main module for the associated process.</summary>    
        [DllImport("kernel32.dll")]
        public static extern IntPtr GetModuleHandle(string name);

        /// <summary> 获取键状态</summary>
        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true,CallingConvention = CallingConvention.Winapi)]
        public static extern short GetKeyState(int keyCode);

        /// <summary>发送键盘按键</summary>
        [DllImport("user32.dll")]
        public static extern void keybd_event(byte key, byte scan, int flags, int extraInfo);

        /// <summary>查找窗体句柄</summary>
        [DllImport("user32.dll", EntryPoint = "FindWindow")]
        public static extern int FindWindow(string lpClassName,string lpWindowName);

        /// <summary>//获取窗体句柄，hwnd为源窗口句柄</summary>
        [DllImport("user32.dll", EntryPoint = "GetWindow")]
        public static extern int GetWindow(int hwnd, int wCmd);
        /// <summary>设置父窗体</summary>
        [DllImport("user32.dll", EntryPoint = "SetParent")]
        public static extern int SetParent(int hWndChild, int hWndNewParent);

        /// <summary>获取鼠标坐标</summary>
        [DllImport("user32.dll", EntryPoint = "GetCursorPos")]
        public static extern int GetCursorPos(ref Point lpPoint);

        /// <summary>指定坐标处窗体句柄</summary>
        [DllImport("user32.dll", EntryPoint = "WindowFromPoint")]
        public static extern int WindowFromPoint(int xPoint, int yPoint);
        /// <summary>往窗体推送信息</summary>
        [DllImport("user32.dll", EntryPoint = "PostMessage", CallingConvention = CallingConvention.Winapi)]
        public static extern bool PostMessage(IntPtr hwnd, int msg, uint wParam, uint lParam);
        /// <summary>往窗体发送信息</summary>
        [DllImport("user32.dll", EntryPoint = "SendMessage", CallingConvention = CallingConvention.Winapi)]
        public static extern bool SendMessage(IntPtr hwnd, int msg, uint wParam, uint lParam);

        /// <summary>获取键盘状态</summary>
        [DllImport("user32.dll", EntryPoint = "GetKeyboardState")]
        public static extern int GetKeyboardState(byte[] pbKeyState);
        /// <summary>获取激活窗体句柄</summary>
        [DllImport("user32.dll")]
        public static extern IntPtr GetActiveWindow();
        /// <summary>激活窗体</summary>
        [DllImport("user32.dll")]
        public static extern IntPtr SetActiveWindow(IntPtr hwnd);//激活窗体

        /// <summary>字符转Keys,不支持大小写</summary>
        [DllImport("user32.dll")]
        public static extern Keys VkKeyScan(char ch);

        /// <summary>字符转Keys,支持大小写</summary>
        [DllImport("user32.dll")]
        public static extern Keys VkKeyScanEx(char ch, long ddd);

        /// <summary>获取当前正在输入的窗口的输入法句柄</summary>
        [DllImport("imm32.dll")]
        public static extern IntPtr ImmGetContext(IntPtr hwnd);

        /// <summary>设置输入法的状态</summary>
        [DllImport("imm32.dll")]
        public static extern bool ImmSetOpenStatus(IntPtr himc, bool b);

        /// <summary>释放输入上下文和解锁相关联的内存</summary>
        [DllImport("imm32.dll")]
        public static extern bool ImmReleaseContext(IntPtr hWnd, IntPtr hIMC);
    }
}
