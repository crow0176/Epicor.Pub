﻿using System;
using System.Data;

namespace Epicor.Pub
{
    ///<summary>数据模型类</summary>
    public class PubDataModel
    {
        ///<summary>工单发料数据模型</summary>
        public static PubColumn[] IssueJobMTL()
        {
            return new PubColumn[] {
                    new PubColumn("PartNum") { ColumnType=typeof(string) }
                    ,new PubColumn("PartPartDescription"){ ColumnType=typeof(string) }
                    ,new PubColumn("TOJobNum"){ ColumnType=typeof(string) }
                    ,new PubColumn("ToAssemblySeq"){ ColumnType=typeof(int) }
                    ,new PubColumn("ToJobPartNum"){ ColumnType=typeof(string) }
                    ,new PubColumn("ToJobPartDescription"){ ColumnType=typeof(string) }
                    ,new PubColumn("ToJobSeq"){ ColumnType=typeof(int) }
                    ,new PubColumn("ToJobSeqPartNum"){ ColumnType=typeof(string) }
                    ,new PubColumn("ToJobSeqPartDesc"){ ColumnType=typeof(string) }
                    ,new PubColumn("TranQty"){ ColumnType=typeof(decimal) }
                    ,new PubColumn("UM"){ ColumnType=typeof(string) }
                    ,new PubColumn("TranReference"){ ColumnType=typeof(string) }
                    ,new PubColumn("LotNum"){ ColumnType=typeof(string) }
                    ,new PubColumn("Company"){ ColumnType=typeof(string) }
                    ,new PubColumn("TranDate"){ ColumnType=typeof(DateTime) }
                    ,new PubColumn("FromWarehouseCode"){ ColumnType=typeof(string) }
                    ,new PubColumn("FromBinNum"){ ColumnType=typeof(string) }
                    ,new PubColumn("ToWarehouseCode"){ ColumnType=typeof(string) }
                    ,new PubColumn("ToBinNum"){ ColumnType=typeof(string) }
                };
        }
        ///<summary>工单退料数据模型</summary>
        public static PubColumn[] ReturnJobMTL()
        {
            return new PubColumn[] {
                    new PubColumn("PartNum"){ ColumnType=typeof(string) }
                    ,new PubColumn("PartPartDescription"){ ColumnType=typeof(string) }
                    ,new PubColumn("FromJobNum"){ ColumnType=typeof(string) }
                    ,new PubColumn("FromAssemblySeq"){ ColumnType=typeof(int) }
                    ,new PubColumn("FromJobPartNum"){ ColumnType=typeof(string) }
                    ,new PubColumn("FromJobPartDescription"){ ColumnType=typeof(string) }
                    ,new PubColumn("FromJobSeq"){ ColumnType=typeof(int) }
                    ,new PubColumn("FromJobSeqPartNum"){ ColumnType=typeof(string) }
                    ,new PubColumn("FromJobSeqPartDesc"){ ColumnType=typeof(string) }
                    ,new PubColumn("TranQty"){ ColumnType=typeof(decimal) }
                    ,new PubColumn("UM"){ ColumnType=typeof(string) }
                    ,new PubColumn("TranReference"){ ColumnType=typeof(string) }
                    ,new PubColumn("LotNum"){ ColumnType=typeof(string) }
                    ,new PubColumn("Company"){ ColumnType=typeof(string) }
                    ,new PubColumn("TranDate"){ ColumnType=typeof(DateTime) }
                    ,new PubColumn("ToWarehouseCode"){ ColumnType=typeof(string) }
                    ,new PubColumn("ToBinNum"){ ColumnType=typeof(string) }
                };
        }
        ///<summary>杂项发料数据模型</summary>
        public static PubColumn[] IssueMiscMTL()
        {
            return new PubColumn[] {
                    new PubColumn("PartNum"){ ColumnType=typeof(string) }
                    ,new PubColumn("PartPartDescription"){ ColumnType=typeof(string) }
                    ,new PubColumn("TranQty"){ ColumnType=typeof(decimal) }
                    ,new PubColumn("UM"){ ColumnType=typeof(string) }
                    ,new PubColumn("TranReference"){ ColumnType=typeof(string) }
                    ,new PubColumn("LotNum"){ ColumnType=typeof(string) }
                    ,new PubColumn("Company"){ ColumnType=typeof(string) }
                    ,new PubColumn("TranDate"){ ColumnType=typeof(DateTime) }
                    ,new PubColumn("FromWarehouseCode"){ ColumnType=typeof(string) }
                    ,new PubColumn("FromBinNum"){ ColumnType=typeof(string) }
                    ,new PubColumn("ReasonCode"){ ColumnType=typeof(string) }
                };

        }
        ///<summary>杂项退料数据模型</summary>
        public static PubColumn[] ReturnMiscMTL()
        {
            return new PubColumn[] {
                    new PubColumn("PartNum"){ ColumnType=typeof(string) }
                    ,new PubColumn("PartPartDescription"){ ColumnType=typeof(string) }
                    ,new PubColumn("TranQty"){ ColumnType=typeof(decimal) }
                    ,new PubColumn("UM"){ ColumnType=typeof(string) }
                    ,new PubColumn("TranReference"){ ColumnType=typeof(string) }
                    ,new PubColumn("LotNum"){ ColumnType=typeof(string) }
                    ,new PubColumn("Company"){ ColumnType=typeof(string) }
                    ,new PubColumn("TranDate"){ ColumnType=typeof(DateTime) }
                    ,new PubColumn("ToWarehouseCode"){ ColumnType=typeof(string) }
                    ,new PubColumn("ToBinNum"){ ColumnType=typeof(string) }
                    ,new PubColumn("ReasonCode"){ ColumnType=typeof(string) }
                };

        }
        ///<summary>杂项退料数据模型</summary>
        public static PubColumn[] BOM()
        {
            return new PubColumn[] {
                    new PubColumn("Symbol"){ ColumnType=typeof(string) }
                    ,new PubColumn("Level"){ ColumnType=typeof(string) }
                    ,new PubColumn("MtlSeq"){ ColumnType=typeof(int) }
                    ,new PubColumn("Type"){ ColumnType=typeof(string) }
                    ,new PubColumn("PartNum"){ ColumnType=typeof(string) }
                    ,new PubColumn("RevisionNum"){ ColumnType=typeof(string) }
                    ,new PubColumn("PartDesc"){ ColumnType=typeof(string) }
                    ,new PubColumn("Qty"){ ColumnType=typeof(decimal) }
                    ,new PubColumn("UOMCode"){ ColumnType=typeof(string) }
                };
        }

        ///<summary>检查数据表是否缺少列</summary>
        public static string CheckColumn(PubColumn[] DCol, DataTable dt) {
            if (!dt.Columns.Contains("ResultMsg"))dt.Columns.Add("ResultMsg", typeof(string));
            string msg = "";
            for (int i = 0; i < DCol.Length; i++)
            {
                if (!dt.Columns.Contains(DCol[i].ColumnName))
                {
                    if (msg != "") msg += "、";
                    msg += DCol[i].ColumnName;
                }
            }
            if (msg != "") return "数据中缺少" + msg + "列!";
            else return "";
        }

        ///<summary>PubColumn[]转DataTable</summary>
        public static DataTable ToDataTable(PubColumn[] cols,string tableName="Table1") {
            DataTable dt = new DataTable(tableName);
            for (int i = 0; i < cols.Length; i++) {
                dt.Columns.Add(cols[i].ToDataColumn());
            }
            return dt;
        }
    }       
}
