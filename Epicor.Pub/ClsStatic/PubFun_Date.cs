﻿using System;

namespace Epicor.Pub
{
    /// <summary>与日期相关的公共方法</summary>
    public static class PubFun_Date
    {
        /// <summary>取得某月的第一天 </summary>
        public static DateTime FirstDayOfMonth(DateTime datetime)
        {
            return datetime.AddDays(1 - datetime.Day);
        }
        /// <summary>取得某月的最后一天 </summary>
        public static DateTime LastDayOfMonth(DateTime datetime)
        {
            return datetime.AddDays(1 - datetime.Day).AddMonths(1).AddDays(-1);
        }


        /// <summary>取得本周第一天 </summary>
        public static DateTime FirstDayOfThisWeek()
        {
            int weeknow = Convert.ToInt32(System.DateTime.Now.DayOfWeek);
            int daydiff = (-1) * weeknow + 1;
            return System.DateTime.Now.AddDays(daydiff);
        }

        /// <summary>取得本周最后一天</summary>
        public static DateTime LastDayOfThisWeek()
        {
            int weeknow = Convert.ToInt32(System.DateTime.Now.DayOfWeek);
            int dayadd = 7 - weeknow;
            return System.DateTime.Now.AddDays(dayadd);
        }


    }
}
