﻿using Ice.Core;
using Ice.Lib.Framework;
using System.Data;

namespace Epicor.Pub
{
    /// <summary>公共静态方法</summary>
    public static class PubFun_HT
    { 
        /// <summary>从Ice.UD10表加载DropDown数据</summary>
        public static void LoadDropDown(EpiTransaction otran, EpiUltraCombo comControl,string dropDownType) {
            PubDB db = new PubDB(otran);
            string SqlStr = "select ShortChar02 as Value,ShortChar03 as Text From Ice.UD10 where (Company='"+ ((Session)otran.Session).CompanyID+"' or CheckBox01='1') and ShortChar01='" + dropDownType + "'";
            DataTable dt = db.ExecuteDt(SqlStr);
            PubFun.LoadDropDown(comControl, dt, "Value", "Text");
        }
    }
}
