﻿using System;
using System.Data;
using Ice.Core;
using Ice.Lib.Framework;

namespace Epicor.Pub
{
    /// <summary>用户操作类</summary>
    public class PubUser
    {
        /// <summary>使用配置文件登录Epicor </summary>
        public static Session Login(string confPath, string user, string password, string company = "", Session.LicenseType licenseType = Session.LicenseType.Default)
        {
            try
            {
                Session _session = new Session(user, password, licenseType, confPath);
                if (company != "") _session.CompanyID = company;
                return _session;
            }
            catch (Exception e1)
            {
                throw new Exception("Login Failed!" + e1.Message);
            }
        }
        /// <summary>退出Epicor登录 </summary>
        public static void Logout(Session session)
        {
            try
            {
                session.OnSessionClosing();
                session.Dispose();
            }
            catch (Exception e1)
            {
                throw new Exception("Logout Failed!" + e1.Message);
            }
        }
        /// <summary>验证用户群组权限</summary>
        public static bool VerifyUserGroup(EpiTransaction otran, string groupID)
        {
            bool result = false;
            PubAdapter adp = new PubAdapter(otran, "Ice.Adapters.UserFile.dll", "Ice.Adapters.UserFileAdapter");
            try
            {
                adp.BOConnect();
                bool bl = adp.GetByID(new object[] { ((Session)otran.Session).UserID }, new Type[] { typeof(string) });
                if (bl)
                {
                    DataSet ds = adp.GetCurrentDataSet();
                    string groupList = ds.Tables["UserFile"].Rows[0]["GroupList"].ToString();
                    result = groupList.Contains(groupID);
                }
            }
            finally
            {
                adp.Dispose();
            }
            return result;
        }
        /// <summary>验证用户公司权限</summary>
        public static bool VerifyUserCompany(EpiTransaction otran, string companyID)
        {
            bool result = false;
            PubAdapter adp = new PubAdapter(otran, "Ice.Adapters.UserFile.dll", "Ice.Adapters.UserFileAdapter");
            try
            {
                adp.BOConnect();
                bool bl = adp.GetByID(new object[] { ((Session)otran.Session).UserID }, new Type[] { typeof(string) });
                if (bl)
                {
                    DataSet ds = adp.GetCurrentDataSet();
                    string companyList = ds.Tables["UserFile"].Rows[0]["CompList"].ToString();
                    result = companyList.Contains(companyID);
                }
            }
            finally
            {
                adp.Dispose();
            }
            return result;
        }
    }
}
