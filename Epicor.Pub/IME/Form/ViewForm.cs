﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Epicor.Pub
{
    /// <summary>ViewForm窗口</summary>
    public partial class ViewForm : Form
    {
        /// <summary>实例化窗口</summary>
        public ViewForm()
        {
            InitializeComponent();
        }
        /// <summary>窗体Load事件</summary>
        private void ViewForm_Load(object sender, EventArgs e)
        {
            if (File.Exists(Application.StartupPath + @"\epicor.ico"))
            {
                this.Icon = new Icon(Application.StartupPath + @"\epicor.ico", 16, 16);
            }
        }
        /// <summary>加载数据</summary>
        public void LoadData(KeyValue[] data)
        {
            comboBox1.Items.Clear();
            if (data != null) 
            {
                for (int i = 0; i < data.Length; i++)
                {
                    comboBox1.Items.Add(data[i]);
                }
            }
        }

        private void ViewStr(string str) {
            tabPage2.Controls.Clear();
            TextBox text1 = new TextBox();
            text1.Text = str;
            text1.Dock = DockStyle.Fill;
            text1.Multiline = true;
            tabPage2.Controls.Add(text1);
        }
        private void ViewTable(DataTable dt)
        {
            tabPage2.Controls.Clear();
            DataGridView dataGridView1 = new DataGridView();
            dataGridView1.Dock = DockStyle.Fill;
            dataGridView1.DataSource = dt;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToDeleteRows = false;
            dataGridView1.RowStateChanged += DataGridView1_RowStateChanged;
            tabPage2.Controls.Add(dataGridView1);
        }

        private void DataGridView1_RowStateChanged(object sender, DataGridViewRowStateChangedEventArgs e)
        {
            e.Row.HeaderCell.Value = string.Format("{0}", e.Row.Index + 1);
        }

        private void comboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                object obj = ((KeyValue)((ComboBox)sender).SelectedItem).Value;
                if (obj == null)
                {
                    ViewStr("null");
                    textBox1.Text = "null";
                }
                else
                {
                    Type type = obj.GetType();
                    textBox1.Text = type.FullName;
                    if (type.IsValueType || type.Name == "String")
                    {
                        ViewStr(obj.ToString());
                    }
                    else if (type.IsArray)
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add("数组列表");
                        for (int i = 0; i < ((Array)obj).Length; i++)
                        {
                            DataRow dr = dt.NewRow();
                            dr["数组列表"] = ((Array)obj).GetValue(i);
                            dt.Rows.Add(dr);
                        }
                        ViewTable(dt);
                    }
                    else if (type.Name == "DataTable")
                    {
                        ViewTable(((DataTable)obj).Copy());
                    }

                }
            }
            catch (Exception e1) {
                MessageBox.Show(e1.Message);
            }
        }

        private void ViewForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Visible = false;
            e.Cancel = true;
        }
    }
}
