﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Epicor.Pub;

namespace Epicor.IME
{
    public partial class IMEMain : Form
    {
        /// <summary>实例化imeFun </summary>
        public IMEFun imeFun;
        /// <summary>代码输入主窗体构造函数</summary>
        public IMEMain(object obj = null)
        {
            InitializeComponent();
            this.MouseDown += new MouseEventHandler(IME_MouseDown);
            this.MouseMove += new MouseEventHandler(IME_MouseMove);
            this.Disposed += IMEMain_Disposed;
            imeFun = new IMEFun();
            imeFun.KeybHook = new HookLib();
            imeFun.IMESelectF = new IMESelect(imeFun);
            imeFun.IMESelectF.Owner = this;
            imeFun.KeybHook.InstallHook(HookMethod);
            Type type = null;
            if (obj != null) type = obj.GetType();
            else {
                StackTrace trace = new StackTrace();
                type = trace.GetFrame(1).GetMethod().DeclaringType;
            }
            imeFun.WaitHashAdd("this", type);
        }

        private void IMEMain_Disposed(object sender, EventArgs e)
        {
            imeFun.IMESelectF.Dispose();
            imeFun.KeybHook.UninstallHook();
        }

        private void IMEMain_Load(object sender, EventArgs e)
        {
           
        }
        bool shiftKey = false;
        bool ctrlKey = false;
        string str123 = "";
        private bool HookMethod(HookLib.HookStruct param, int wParam)
        {
            try
            {
                int WM_KEYDOWN = 0x100;
                int WM_KEYUP = 0x101;
                int WM_SYSKEYDOWN = 0x104;
                int WM_SYSKEYUP = 0x105;
                if (imeFun.canSetKey) return false;
                Keys keyData = (Keys)param.vkCode;
                if (wParam == WM_KEYDOWN || wParam == WM_SYSKEYDOWN) //按下键
                {
                    if (keyData == Keys.F3)
                    {
                        if (this.Visible)
                        {
                            this.Hide();
                            imeFun.InitData();
                        }
                        else
                        {
                            IntPtr activeForm = WinAPI32.GetActiveWindow(); // 先得到当前的活动窗体 
                            imeFun.InitData();
                            this.Show();
                            this.SetDesktopLocation(1, SystemInformation.WorkingArea.Height - this.Height);
                            WinAPI32.SetActiveWindow(activeForm); // 在把焦点还给之前的活动窗体
                        }
                        return true;
                    }
                    if (this.Visible)
                    {
                        if (ctrlKey)
                        {
                            if (keyData >= Keys.D0 && keyData <= Keys.D9)
                            {
                                int idx = keyData.GetHashCode() - 49;
                                if (keyData == Keys.D0) idx = 9;
                                imeFun.SetIndex(idx);
                                imeFun.SetChar(imeFun.GetSelectText());
                            }
                            else {
                                switch (keyData)
                                {
                                    case Keys.OemMinus:
                                        imeFun.PrevPage();
                                        break;
                                    case Keys.Oemplus:
                                        imeFun.NextPage();
                                        break;
                                    default:
                                        return false;
                                }
                            }
                        }
                        else
                        {
                            bool capsLock = imeFun.KeybHook.CapsLockStatus;
                            string keyStr = imeFun.GetKeyStr(keyData, shiftKey, capsLock);
                            str123 += keyData.GetHashCode().ToString() + ",";
                            if (!string.IsNullOrEmpty(keyStr))
                            {

                                imeFun.AddChar(keyStr);
                            }
                            else if (keyData == Keys.LShiftKey || keyData == Keys.RShiftKey)
                            {
                                shiftKey = true;
                                return false;
                            }
                            else if (keyData == Keys.LControlKey || keyData == Keys.RControlKey)
                            {
                                ctrlKey = true;
                                return false;
                            }
                            else if (imeFun.IMESelectF.Visible)
                            {
                                switch (keyData)
                                {
                                    case Keys.Back:
                                        imeFun.BackChar();
                                        break;
                                    case Keys.Delete:
                                        imeFun.DeleteChar();
                                        break;
                                    case Keys.Left:
                                        imeFun.MoveChar(-1);
                                        break;
                                    case Keys.Right:
                                        imeFun.MoveChar(1);
                                        break;
                                    case Keys.Up:
                                        imeFun.PrevIndex();
                                        break;
                                    case Keys.Down:
                                        imeFun.NextIndex();
                                        break;
                                    case Keys.PageUp:
                                        imeFun.PrevPage();
                                        break;
                                    case Keys.PageDown:
                                        imeFun.NextPage();
                                        break;
                                    case Keys.Enter:
                                        imeFun.SendText();
                                        break;
                                    case Keys.Space:
                                        imeFun.SendText(imeFun.inputStr);
                                        break;
                                    case Keys.Tab:
                                        imeFun.SetChar(imeFun.GetSelectText());
                                        break;
                                    default:
                                        return false;
                                }
                            }
                            else return false;
                        }
                        return true;
                    }
                }
                else if (wParam == WM_KEYUP || wParam == WM_SYSKEYUP)//抬起键
                {
                    if (this.Visible)
                    {
                        if (keyData == Keys.LShiftKey || keyData == Keys.RShiftKey)
                        {
                            shiftKey = false;
                        }
                        else if (keyData == Keys.LControlKey || keyData == Keys.RControlKey)
                        {
                            ctrlKey = false;
                        }
                    }
                }

            }
            catch (Exception e1) {
                MessageBox.Show(e1.Message);
            }
            return false;
        }



        private void SetKeyData(Keys keyData) {

        }
        private Point offset;
        private void IME_MouseDown(object sender, MouseEventArgs e)
        {
            if (MouseButtons.Left != e.Button) return;
            Point cur = this.PointToScreen(e.Location);
            offset = new Point(cur.X - this.Left, cur.Y - this.Top);
        }
        private void IME_MouseMove(object sender, MouseEventArgs e)
        {
            if (MouseButtons.Left != e.Button) return;
            Point cur = Form.MousePosition;
            this.Location = new Point(cur.X - offset.X, cur.Y - offset.Y);
        }

        private void IMEMain_Paint(object sender, PaintEventArgs e)
        {
            int n = 4;
            Graphics g = e.Graphics;
            g.Clear(this.BackColor);
            ControlPaint.DrawBorder(g, this.ClientRectangle,
               Color.DodgerBlue, 1, ButtonBorderStyle.Solid,
               Color.DodgerBlue, 1, ButtonBorderStyle.Solid,
               Color.DodgerBlue, 1, ButtonBorderStyle.Solid,
               Color.DodgerBlue, 1, ButtonBorderStyle.Solid);
            SolidBrush bru1 = new SolidBrush(Color.DodgerBlue);
            Font myFont = new Font("宋体", 11, FontStyle.Bold);
            g.DrawString("代码提示工具", myFont, bru1, new RectangleF(n, n, this.Width, this.Height));
            g.Dispose();
        }
    }
}
