﻿namespace Epicor.IME
{
    /// <summary> </summary>
    partial class IMEMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // IMEMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(108, 23);
            this.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximumSize = new System.Drawing.Size(108, 23);
            this.MinimumSize = new System.Drawing.Size(108, 23);
            this.Name = "IMEMain";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "IMEMain";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.IMEMain_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.IMEMain_Paint);
            this.ResumeLayout(false);

        }

        #endregion
    }
}