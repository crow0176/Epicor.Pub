﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Epicor.IME
{
    public partial class IMESelect : Form
    {
        private IMEFun imeFun;
        /// <summary>选择列表窗体构造函数</summary>
        public IMESelect(IMEFun obj)
        {
            InitializeComponent();
            imeFun = obj;
            panel3.MouseDown += new MouseEventHandler(IME_MouseDown);
            panel3.MouseMove += new MouseEventHandler(IME_MouseMove);
            panel2.MouseDown += new MouseEventHandler(IME_MouseDown);
            panel2.MouseMove += new MouseEventHandler(IME_MouseMove);
            Font myFont = new Font("Arial", 9);
            for (int i = 0;i< imeFun.maxShow;i++)
            {
                Label lb = new Label() { Text = "", Font = myFont, AutoSize = true };
                lb.Location = new Point(1,i*18);
                //lb.MouseEnter += Lb_MouseEnter;
                panel3.Controls.Add(lb);
            }
        }

        //private ToolTip tooltip= new ToolTip();
        //private void Lb_MouseEnter(object sender, EventArgs e)
        //{
        //    foreach (Control c in panel3.Controls) {
        //        Label lb1 = (Label)c;
        //        tooltip.Hide(lb1);
        //    }
        //    Label lb = (Label)sender;
        //    if (lb.Tag != null)
        //    {
        //        tooltip.Show(lb.Tag.ToString(), lb);
        //    }
        //}

        private void IME_Load(object sender, EventArgs e)
        {
            int w = SystemInformation.WorkingArea.Width;
            int h = SystemInformation.WorkingArea.Height;
            this.SetDesktopLocation(1, h - this.Height - 30);
        }
        /// <summary>绘制输入的字符串及光标</summary>
        public void DrawStr() {
            this.Visible = !string.IsNullOrEmpty(imeFun.inputStr);
            if (this.Visible)
            {
                int n = 4;
                int h = 16;
                Graphics g = panel2.CreateGraphics();
                g.Clear(panel2.BackColor);
               
                Font myFont = new Font("Arial", 10);
                //SizeF sizeF1 = g.MeasureString(imeFun.inputStr, myFont, 10000, StringFormat.GenericTypographic);
                SizeF sizeF1 = TextRenderer.MeasureText(imeFun.inputStr, myFont);
                if ((int)sizeF1.Width + 50 > this.Width) this.Width = (int)sizeF1.Width + 50;
                //SizeF sizeF = g.MeasureString(imeFun.inputStr.Substring(0,imeFun.cursorIndex), myFont, 10000, StringFormat.GenericTypographic);
                //g.DrawString(imeFun.inputStr, myFont, bru1, new RectangleF(n, n, panel2.Width, panel2.Height));
               
                TextRenderer.DrawText(g, imeFun.inputStr,myFont,new Point(n,n), Color.DodgerBlue);
                SolidBrush bru1 = new SolidBrush(Color.DodgerBlue);
                SizeF sizeF = TextRenderer.MeasureText(imeFun.inputStr.Substring(0, imeFun.cursorIndex), myFont);
                g.DrawLine(new Pen(bru1), sizeF.Width, n, sizeF.Width, h + n);
                g.DrawLine(new Pen(bru1), n, h + n + 2, panel2.Width - 2 * n, h + n + 2);
                ControlPaint.DrawBorder(g, panel2.ClientRectangle,
                   Color.DodgerBlue, 1, ButtonBorderStyle.Solid,
                   Color.DodgerBlue, 1, ButtonBorderStyle.Solid,
                   Color.DodgerBlue, 1, ButtonBorderStyle.Solid,
                   Color.DodgerBlue, 0, ButtonBorderStyle.Solid);
                g.Dispose();
            }
        }
        /// <summary>加载要显示的选择列表</summary>
        public void LoadSelectList()
        {
            Font myFont = new Font("Arial", 11);
            imeFun.currShow = 0;
            int maxWidth = 0;
            for (int i = 0; i < imeFun.maxShow; i++) {
                int index = imeFun.maxShow * imeFun._selectPage + i;
                Label lb = (Label)panel3.Controls[i];
                lb.Text = "";
                if (imeFun.selectList.Count > index)
                {
                    imeFun.currShow++;
                    WaitData kv = (WaitData)imeFun.selectHash[imeFun.selectList[index]];

                    lb.Text = ((i + 1)% imeFun.maxShow).ToString() + ". " + kv.key + (string.IsNullOrEmpty(kv.note)? "": " //"+kv.note);
                    //lb.Tag= kv.note;
                    lb.ForeColor = Color.Black;
                    if (imeFun._selectIndex == i) lb.ForeColor = Color.DodgerBlue;
                }
               if (lb.Width > maxWidth) maxWidth = lb.Width;
            }
            if (maxWidth <200) this.Width = 200;
            else this.Width = maxWidth+5;
        }
        /// <summary>加载要显示的选择列表</summary>
        public void SetSelectLable()
        {
            for (int i = 0; i < panel3.Controls.Count; i++) {
                Label lb = (Label)panel3.Controls[i];
                if (imeFun._selectIndex == i) lb.ForeColor = Color.DodgerBlue;
                else  lb.ForeColor = Color.Black;
            }
        }

        private Point offset;
        private void IME_MouseDown(object sender, MouseEventArgs e)
        {
            if (MouseButtons.Left != e.Button) return;
            Point cur = Control.MousePosition;
            offset = new Point(cur.X - this.Left, cur.Y - this.Top);
        }
        private void IME_MouseMove(object sender, MouseEventArgs e)
        {
            if (MouseButtons.Left != e.Button) return;
            Point cur = Control.MousePosition;
            this.Location = new Point(cur.X - offset.X, cur.Y - offset.Y);
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            DrawStr();
        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = panel3.CreateGraphics();
            g.Clear(panel3.BackColor);
            ControlPaint.DrawBorder(g, panel3.ClientRectangle,
               Color.DodgerBlue, 1, ButtonBorderStyle.Solid,
               Color.DodgerBlue, 0, ButtonBorderStyle.Solid,
               Color.DodgerBlue, 1, ButtonBorderStyle.Solid,
               Color.DodgerBlue, 1, ButtonBorderStyle.Solid);
        }
    }
}
