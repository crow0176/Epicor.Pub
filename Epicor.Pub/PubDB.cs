﻿using Ice.Core;
using Ice.Lib.Framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epicor.Pub
{
    /// <summary>数据库操作类</summary>
    public class PubDB:SqlDB
    {
        /// <summary>数据库操作类构造函数</summary>
        public PubDB(EpiTransaction otran)
        {
            ConnStr = PubFun.GetConnStr();
        }
         
        #region Epicor常用查询
        /// <summary> 获取新ID</summary>
        /// <param name="idNum">返回取得的ID数值</param>
        /// <param name="companyID">公司</param>
        /// <param name="tableName">表名</param>
        /// <param name="numberColumn">存储ID数值的列名</param>
        /// <param name="whereClause">筛选条件</param>
        /// <param name="numLength">ID字符串位数不够该值时,前面加0</param>
        /// <returns>返回取得的ID字符串</returns>
        public string GetNewID(out int idNum,string companyID,string tableName, string numberColumn, string whereClause="", int numLength=5)
        {
            if (whereClause != "" && !whereClause.Trim().ToLower().StartsWith("and "))
            {
                whereClause = "AND " + whereClause;
            }
            string SqlStr = "select isnull(Max(Convert(int," + numberColumn + ")),0)+1 from " + tableName + " where Company='" + companyID + "' " + whereClause + ";";
            idNum = Convert.ToInt32(ExecuteScalar(SqlStr));
            return String.Format("{0:D" + (numLength <= 0 ? "" : numLength.ToString()) + "}", idNum);
        }

        /// <summary>获取数据库服务器时间</summary>
        public DateTime GetDBTime()
        {
            string SqlStr = "select getdate();";
            DateTime now = Convert.ToDateTime(ExecuteScalar(SqlStr));
            return now;
        }
        #endregion
    }
}
