﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Epicor.Pub
{
    public partial class UCReportView : UserControl
    {
        /// <summary>报表显示器</summary>
        public UCReportView()
        {
            InitializeComponent();
        }
        /// <summary>刷新</summary>
        public void RefreshReport() {
            this.ReportViewer.RefreshReport();
        }
        /// <summary>设置服务器报表</summary>
        public void SetServerReport(string ReportPath, string ReportServerUrl = null)
        {
            if (String.IsNullOrEmpty(ReportServerUrl))
            {
                ReportServerUrl = PubFun.Conf.SSRS_ServerUrl;
            }
            this.ReportViewer.ProcessingMode = Microsoft.Reporting.WinForms.ProcessingMode.Remote;
            this.ReportViewer.ServerReport.ReportServerUrl = new System.Uri(ReportServerUrl);
            this.ReportViewer.ServerReport.ReportPath = ReportPath;
            this.ReportViewer.ShowParameterPrompts = false;
            this.RefreshReport();
             
        }
    }
}
