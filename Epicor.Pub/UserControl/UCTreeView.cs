﻿using System;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace Epicor.Pub
{
    public partial class UCTreeView : UserControl
    {
        /// <summary>节点选中委托</summary> 
        public delegate void AfterSelectNodeHandle(TreeNode node);
        /// <summary>节点选中事件</summary>
        public event AfterSelectNodeHandle AfterSelectNode;
        /// <summary>节点拖动委托</summary>
        public delegate void DragDropNodeHandle(TreeNode moveNode, TreeNode targeNode);
        /// <summary>节点拖动事件</summary>
        public event DragDropNodeHandle DragDropNode;
        /// <summary>节点删除委托</summary>
        public delegate void DeleteNodeHandle(TreeNode node);
        /// <summary>节点删除事件</summary>
        public event DeleteNodeHandle DeleteNode;
        /// <summary>子节点新建委托</summary>
        public delegate void NewChildNodeHandle(TreeNode pNode);
        /// <summary>子节点新建事件</summary>
        public event NewChildNodeHandle NewChildNode;

        private DataTable TreeData;
        /// <summary>节点显示列列名</summary>
        public string TextCol = "Text";
        /// <summary>节点主键列名</summary>
        public string KeyCol = "Key";
        /// <summary>父节点主键列名</summary>
        public string PKeyCol = "PKey";

        /// <summary>当前选中节点</summary>
        public TreeNode SelectedNode {
            get {
                return treeView1.SelectedNode;
            }
        }


        /// <summary>控件构造函数 </summary>
        public UCTreeView()
        {
            InitializeComponent();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            AfterSelectNode?.Invoke(e.Node);
        }
        /// <summary>控件加载数据 </summary>
        public void LoadData(DataTable dt, string pKey="") {
            treeView1.Nodes.Clear();
            if (dt != null) {
                TreeData = dt;
                LoadTreeNode(pKey);
                treeView1.ExpandAll();
            }
        }
        private void LoadTreeNode(string pKey,TreeNode treeNode=null)
        {
            DataRow[] drs = TreeData.Select(PKeyCol+"='" + pKey + "'");
            for (int i = 0; i < drs.Length; i++)
            {
                TreeNode treeNode1 = new TreeNode(drs[i][TextCol].ToString())
                {
                    Name = drs[i][KeyCol].ToString()
                    ,Tag = drs[i]
                };
                if (treeNode == null) 
                {
                    treeView1.Nodes.Add(treeNode1);
                }
                else {
                    treeNode.Nodes.Add(treeNode1);
                }
                LoadTreeNode(drs[i][KeyCol].ToString(),treeNode1);
            }
        }

        private void treeView1_DragDrop(object sender, DragEventArgs e)
        {
            TreeNode moveNode = (TreeNode)e.Data.GetData(typeof(TreeNode));
            //根据鼠标坐标确定要移动到的目标节点
            Point pt = ((TreeView)(sender)).PointToClient(new Point(e.X, e.Y));
            TreeNode targeNode = treeView1.GetNodeAt(pt);
            DragDropNode?.Invoke(moveNode, targeNode);
        }

        private void treeView1_ItemDrag(object sender, ItemDragEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                DoDragDrop(e.Item, DragDropEffects.Move);
            }
        }

        private void treeView1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(TreeNode)))
                e.Effect = DragDropEffects.Move;
            else
                e.Effect = DragDropEffects.None;
        }

        private void MenuItemNew_Click(object sender, EventArgs e)
        {

            if (treeView1.SelectedNode != null|| treeView1.Nodes.Count == 0)
            {
                NewChildNode?.Invoke(treeView1.SelectedNode);
            }
        }

        private void MenuItemDelete_Click(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode != null)
            {
                DialogResult dialogResult = MessageBox.Show("是否删除当前选中节点及其子节点？", "提示", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.OK)
                {
                    DeleteNode?.Invoke(treeView1.SelectedNode);
                }
            }
        }
    }
}
