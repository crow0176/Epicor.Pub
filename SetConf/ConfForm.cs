﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Epicor.Pub;

namespace SetConf
{
    public partial class ConfForm : Form
    {
        public ConfForm()
        {
            InitializeComponent();
        }
        PubXML xml = null;
        public DataTable dt = new DataTable();
        private void ConfForm_Load(object sender, EventArgs e)
        {
            dt.Columns.AddRange(new DataColumn[] {
                new DataColumn("Type"),
                new DataColumn("Key"),
                new DataColumn("Value"),
                new DataColumn("Encrypt",typeof(bool)),
            });
            LoadFile();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (xml != null) {
				xml.Save();
				LoadFile();
			}
        }
        public void LoadFile() {
            dt.Clear();
            try
            {
                string fileName = "Epicor.Pub.config";
                xml = new PubXML(fileName);
                XmlNode root = xml.GetNode("PubConfig");
                for (int i = 0; i < root.ChildNodes.Count; i++)
                {
                    XmlNode type = root.ChildNodes[i];
                    for (int j = 0; j < type.ChildNodes.Count; j++)
                    {
                        XmlElement key = type.ChildNodes[j] as XmlElement;
                        string str = key.GetAttribute("Encrypt");
                        bool encrypt = false;
                        if (!string.IsNullOrEmpty(str)) encrypt = Convert.ToBoolean(str);
                        DataRow dr = dt.NewRow();
                        dr["Type"] = type.Name;
                        dr["Key"] = key.Name;
                        dr["Value"] = encrypt ? PubFun.Md5Decrypt(key.InnerText) : key.InnerText;
                        dr["Encrypt"] = encrypt;
                        dt.Rows.Add(dr);
                    }
                }
            }
            catch (Exception e1) {
                MessageBox.Show(e1.Message);
            }
            dataGridView1.DataSource = dt;
            dataGridView1.Columns["Type"].ReadOnly = true;
            dataGridView1.Columns["Type"].Width = 80;
            dataGridView1.Columns["Key"].ReadOnly = true;
            dataGridView1.Columns["Key"].Width = 80;
            dataGridView1.Columns["Value"].Width = 200;
            dataGridView1.Columns["Encrypt"].Width = 60;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            LoadFile();
        }

        private void dataGridView1_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            string type = dataGridView1.Rows[e.RowIndex].Cells["Type"].Value.ToString();
            string key = dataGridView1.Rows[e.RowIndex].Cells["Key"].Value.ToString();
            string value = dataGridView1.Rows[e.RowIndex].Cells["Value"].Value.ToString();
            string changColumnName = dataGridView1.Columns[e.ColumnIndex].Name;
            string changedValue = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();
            if (changColumnName == "Value")
            {
                xml.UpdateText("PubConfig/" + type + "/" + key, changedValue);
            }
            else if (changColumnName == "Encrypt")
            {
                xml.UpdateElement("PubConfig/" + type + "/" + key, changColumnName, changedValue);
                xml.UpdateText("PubConfig/" + type + "/" + key, Convert.ToBoolean(changedValue) ? PubFun.Md5Encrypt(value) : value);
            }
            else {
                xml.UpdateElement("PubConfig/" + type + "/" + key, changColumnName, changedValue);
            }
        }
    }
}
