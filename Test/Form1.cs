﻿
using Epicor.Pub;
using Ice.Lib.Framework;
using System;
using System.Data;
using System.Windows.Forms;
namespace Test
{
    public partial class Form1 : Form
    {
        public DataTable dt = new DataTable();
        public string[]  arr = { "123","qwe","dfd"};
        Ice.Core.Session se = null;
        public Form1() 
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {


        }

        private void button1_Click(object sender, EventArgs e)
        {
            se = PubFun.Login(PubFun.Conf.Server_ConfigPath, PubFun.Conf.Server_UserID, PubFun.Conf.Server_Password);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (se != null)
                label1.Text = "已登录,UserID=" + se.UserID + ",CompanyID=" + se.CompanyID;
            else label1.Text = "未登录";
            //if (PubFun.IMEIsStart())label2.Text = "工具已启动";
            //else label2.Text = "工具未启动";

            if (dt != null) label4.Text = "获取" + dt.Rows.Count.ToString() + "行数据!";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (se != null)
            {
                ILauncher IL = new ILauncher(se);
                EpiTransaction otrans = new EpiTransaction(IL);
                PubFun.OpenMenu(otrans, "SE000001");//需要放在Epicor客户端目录下
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (se != null)
            {
                ILauncher IL = new ILauncher(se);
                EpiTransaction otrans = new EpiTransaction(IL);
                Epicor.Pub.BAQWhereItem[] queryWhere = new Epicor.Pub.BAQWhereItem[] {
                    new Epicor.Pub.BAQWhereItem("POHeader", "PONum", "5","int","",">") { Seq=1 }
                };
                dt = PubFun.GetBAQData(otrans, "POQuery", true, queryWhere);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //测试调用快速搜索
            if (se != null)
            {
                ILauncher IL = new ILauncher(se);
                EpiTransaction otrans = new EpiTransaction(IL);
                object obj=  PubFun.OpenQuickSearchForm(otrans, "BaseDataQuery");
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //添加物料仓库
            if (se != null)
            {
                try
                {
                    ILauncher IL = new ILauncher(se);
                    EpiTransaction otrans = new EpiTransaction(IL);
                    PubFun.NewPartWhse(otrans, "123", "WIP");
                    MessageBox.Show("添加完成!");
                }
                catch (Exception e1) {
                    MessageBox.Show(e1.Message);
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            //生成ICPO
            if (se != null)
            {
                try
                {
                    ILauncher IL = new ILauncher(se);
                    EpiTransaction otrans = new EpiTransaction(IL);
                    PubFun.CalculateCTP(otrans, 123);
                    MessageBox.Show("生成完成!");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                }
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            //批量杂项发料
            if (se != null)
            {
                try
                {
                    DataTable dt = PubDataModel.ToDataTable(PubDataModel.IssueMiscMTL());
                    DataRow dr = dt.NewRow();
                    dr["PartNum"] = "BD13011";
                    dr["PartPartDescription"] = "O-RING (dia.68)";
                    dr["TranQty"] = 1;
                    dr["UM"] = "PCS";
                    dr["TranReference"] = "123";
                    dr["LotNum"] = "123";
                    dr["Company"] = "WZMMP";
                    dr["TranDate"] = DateTime.Now;
                    dr["FromWarehouseCode"] = "W32";
                    dr["FromBinNum"] = "00";
                    dr["ReasonCode"] = "030";
                    dt.Rows.Add(dr);
                    ILauncher IL = new ILauncher(se);
                    EpiTransaction otrans = new EpiTransaction(IL);
                    int num=PubFun.IssueMiscMTLMass(otrans, dt);
                    if(num==dt.Rows.Count) MessageBox.Show("发料完成!");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                }
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            //批量工单发料
            if (se != null)
            {
                try
                {
                    DataTable dt = PubDataModel.ToDataTable(PubDataModel.IssueJobMTL());
                    DataRow dr = dt.NewRow();
                    dr["PartNum"] = "BU01003";
                    dr["TOJobNum"] = "000005-1-1";
                    dr["ToAssemblySeq"] = "0";
                    dr["ToJobPartNum"] = "H2111M";
                    dr["ToJobSeq"] = "10";
                    dr["ToJobSeqPartNum"] = "BU01003";
                    dr["TranQty"] = 1;
                    dr["UM"] = "G";
                    dr["TranReference"] = "123";
                    dr["Company"] = "WZMMP";
                    dr["TranDate"] = DateTime.Now;
                    dr["FromWarehouseCode"] = "W31";
                    dr["FromBinNum"] = "00";
                    dr["ToWarehouseCode"] = "WIP";
                    dr["ToBinNum"] = "DC";
                    dt.Rows.Add(dr);
                    ILauncher IL = new ILauncher(se);
                    EpiTransaction otrans = new EpiTransaction(IL);
                    int num = PubFun.IssueJobMTLMass(otrans, dt);
                    if (num == dt.Rows.Count) MessageBox.Show("发料完成!");
                    else MessageBox.Show("发料失败!");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                }
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            //批量杂项退料
            if (se != null)
            {
                try
                {
                    DataTable dt = PubDataModel.ToDataTable(PubDataModel.ReturnMiscMTL());
                    DataRow dr = dt.NewRow();
                    dr["PartNum"] = "BD13011";
                    dr["PartPartDescription"] = "O-RING (dia.68)";
                    dr["TranQty"] = 1;
                    dr["UM"] = "PCS";
                    dr["TranReference"] = "123";
                    dr["LotNum"] = "123";
                    dr["Company"] = "WZMMP";
                    dr["TranDate"] = DateTime.Now;
                    dr["ToWarehouseCode"] = "W32";
                    dr["ToBinNum"] = "00";
                    dr["ReasonCode"] = "030";
                    dt.Rows.Add(dr);
                    ILauncher IL = new ILauncher(se);
                    EpiTransaction otrans = new EpiTransaction(IL);
                    int num = PubFun.ReturnMiscMTLMass(otrans, dt);
                    if (num == dt.Rows.Count) MessageBox.Show("退料完成!");
                    else MessageBox.Show("退料失败!");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                }
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            //批量工单退料
            if (se != null)
            {
                try
                {
                    DataTable dt = PubDataModel.ToDataTable(PubDataModel.ReturnJobMTL());
                    DataRow dr = dt.NewRow();
                    dr["PartNum"] = "BU01003";
                    dr["FromJobNum"] = "000005-1-1";
                    dr["FromAssemblySeq"] = "0";
                    dr["FromJobPartNum"] = "H2111M";
                    dr["FromJobSeq"] = "10";
                    dr["FromJobSeqPartNum"] = "BU01003";
                    dr["TranQty"] = 1;
                    dr["UM"] = "G";
                    dr["TranReference"] = "123";
                    dr["Company"] = "WZMMP";
                    dr["TranDate"] = DateTime.Now;
                    dr["ToWarehouseCode"] = "W31";
                    dr["ToBinNum"] = "00";
                    dt.Rows.Add(dr);
                    ILauncher IL = new ILauncher(se);
                    EpiTransaction otrans = new EpiTransaction(IL);
                    int num = PubFun.ReturnJobMTLMass(otrans, dt);
                    if (num == dt.Rows.Count) MessageBox.Show("退料完成!");
                    else MessageBox.Show("退料失败!");
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                }
            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                string fileName = sfd.FileName;
                if (!string.IsNullOrEmpty(fileName))
                {
                    PubExcel excel = new PubExcel();
                    DataTable dt = new DataTable();
                    dt.Columns.AddRange(new DataColumn[] {
                        new DataColumn("A",typeof(string))
                        ,new DataColumn("B",typeof(string))
                        ,new DataColumn("C",typeof(string))
                        ,new DataColumn("D",typeof(string))
                    });
                    excel.CreateSheet("aaa", dt);
                    excel.Save(fileName);
                }
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                string fileName = ofd.FileName;
                if (!string.IsNullOrEmpty(fileName))
                {
                    PubExcel excel = new PubExcel();
                    DataTable dt = new DataTable();
                    dt.Columns.AddRange(new DataColumn[] {
                        new DataColumn("A",typeof(string))
                        ,new DataColumn("B",typeof(string))
                        ,new DataColumn("C",typeof(string))
                        ,new DataColumn("D",typeof(string))
                    });
                    excel.Load(fileName);
                    excel.ToDataTable(ref dt,"Sheet1",0);
                    dataGridView1.DataSource = dt;
                }
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (se != null)
            {
                string company = "DGMMP";
                ILauncher IL = new ILauncher(se);
                EpiTransaction otrans = new EpiTransaction(IL);
                bool bl= PubUser.VerifyUserCompany(otrans, company);
                if (bl) MessageBox.Show("当前用户有"+ company + "公司权限!");
                else MessageBox.Show("当前用户没有" + company + "公司权限!");
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            if (se != null)
            {
                string group = "OFFICE";
                ILauncher IL = new ILauncher(se);
                EpiTransaction otrans = new EpiTransaction(IL);
                bool bl = PubUser.VerifyUserGroup(otrans, group);
                if (bl) MessageBox.Show("当前用户有"+ group + "群组权限!");
                else MessageBox.Show("当前用户没有" + group + "群组权限!");
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            if (se != null && cmb_Rev.SelectedItem!=null)
            {
                ILauncher IL = new ILauncher(se);
                EpiTransaction otrans = new EpiTransaction(IL);
                PubBOM bom = new PubBOM(otrans);
                bom.LoadPartBOM(tbPartNum.Text, cmb_Rev.SelectedItem.ToString());
                dataGridView2.DataSource = bom.BOMData;
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            if (se != null)
            {
                ILauncher IL = new ILauncher(se);
                EpiTransaction otrans = new EpiTransaction(IL);
                string key1 = textBox1.Text;
                object[] obj = new object[] { new object[] { key1, "", "", "", "" } };
                Type[] types = new Type[] { typeof(object[])};
                if (PubFun.DeleteByID(otrans, "UD05", obj, types)) MessageBox.Show("删除成功!");
                else MessageBox.Show("删除失败!");
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            PubFun.InitIME(this);
        }

        private void button18_Click(object sender, EventArgs e)
        {
            if (se != null)
            {
                ILauncher IL = new ILauncher(se);
                EpiTransaction otrans = new EpiTransaction(IL);
                DataTable dt= PubFun.GetAdapterData(otrans, "PartAdapter", "", true,false);
                if (dt.Rows.Count > 0) {
                    tbPartNum.Text = dt.Rows[0]["PartNum"].ToString();
                }
            }
        }

        private void tbPartNum_TextChanged(object sender, EventArgs e)
        {
            if (se != null&&tbPartNum.Text.Trim()!="")
            {
                ILauncher IL = new ILauncher(se);
                EpiTransaction otrans = new EpiTransaction(IL);
                DataTable dt = PubFun.GetAdapterData(otrans, "PartRevSearchAdapter", "PartNum='"+ tbPartNum.Text.Trim() + "'");
                cmb_Rev.Items.Clear();
                for (int r = 0; r < dt.Rows.Count; r++) {
                    cmb_Rev.Items.Add(dt.Rows[r]["RevisionNum"].ToString());
                }
                
            }
        }

        private void button19_Click_1(object sender, EventArgs e)
        {
            if (se != null)
            {    
                ILauncher IL = new ILauncher(se);
    
                EpiTransaction otrans = new EpiTransaction(IL);

                PubFun.OpenBAQReportForm(otrans, "PackingList", new KeyValue[] { new KeyValue("field1", "41") });

            }
        }

        private void button20_Click(object sender, EventArgs e)
        {
            if (se != null)
            {
                ILauncher IL = new ILauncher(se);
                Erp.UI.App.PartEntry.PartTransaction otrans = new Erp.UI.App.PartEntry.PartTransaction(IL);
                Erp.UI.App.PartEntry.PartForm form = new Erp.UI.App.PartEntry.PartForm(otrans);
                form.CustomizationName = "OT_Part";
                form.Show();

            }
  
        }

        private void button21_Click(object sender, EventArgs e)
        {
            PubFun.SendEmail("wjl0176@qq.com", "wjl0176@qq.com", "52126070@qq.com", "", "山东省十大", "大幅度释放斯蒂芬");
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {
            
        }

        private void button22_Click(object sender, EventArgs e)
        {
            EvaluatorItem item = new EvaluatorItem(typeof(int), "123-432/43*11", "test");
            PubEvaluator eval = new PubEvaluator(item);
            string r = ((int)eval.Evaluate(item.Name)).ToString();
            MessageBox.Show("执行" + item.Expression + "，结果返回" + r);
        }
    }
}
