﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Erp.Proxy.BO;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Ice.Lib.Framework;

namespace Test
{
    public partial class Form2 : Form
    {
        public DataTable dt = new DataTable();
        public Form2()
        { 
            InitializeComponent(); 
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            dt.Columns.Add("Col1");
            dt.Columns.Add("Col2");
            dt.Columns.Add("Col3");
            DataRow dr1 = dt.NewRow();
            dr1["Col1"] = "aaa";
            dr1["Col2"] = "bbb";
            dr1["Col3"] = "ccc";
            dt.Rows.Add(dr1);
            DataRow dr2 = dt.NewRow();
            dr2["Col1"] = "111";
            dr2["Col2"] = "222";
            dr2["Col3"] = "333";
            dt.Rows.Add(dr2);
            EpiUltraGrid ug1 = new EpiUltraGrid();
            ug1.DataSource = dt;
            panel1.Controls.Add(ug1);
        }

    }
}
