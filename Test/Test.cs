﻿using Ice.Lib.Framework;
using Ice.Lib.Searches;
using System;
using System.Data;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace Test
{
    class Test
    {

        public static Type GetAdapterType(string adpName, string className1 = null) {
            string dllfile = "Erp.Adapters." + adpName + ".dll";
            string className = "Erp.Adapters." + adpName + "Adapter";
            if (!File.Exists(Application.StartupPath + @"\" + dllfile))
            {
                dllfile = "Ice.Adapters." + adpName + ".dll";
                className = "Ice.Adapters." + adpName + "Adapter";
            }
            if (!File.Exists(Application.StartupPath + @"\" + dllfile))
            {
                dllfile = adpName;
                className = className1;
            }
            Assembly assembly = Assembly.LoadFrom(Application.StartupPath + @"\" + dllfile);
            return assembly.GetType(className);
        }
        ///<summary>获取适配器数据(反射)</summary>
        public static DataTable GetAdapterData(EpiTransaction otrans, Type Type1, SearchOptions opts, string tableName = null)
        {
            DataTable dt = new DataTable();
            Object obj= Activator.CreateInstance(Type1, new object[] { otrans });
            try
            {
                Type1.GetMethod("BOConnect").Invoke(obj, null);
                DialogResult result = (DialogResult)(Type1.GetMethod("InvokeSearch", new Type[] { typeof(SearchOptions) }).Invoke(obj, new object[] { opts }));
                if (result == DialogResult.OK)
                {
                    DataSet ds1 = (DataSet)(Type1.GetMethod("GetCurrentDataSet").Invoke(obj, null));
                    if (tableName == null)dt = ds1.Tables[0].Copy();
                    else dt = ds1.Tables[tableName].Copy();
                }
            }
            finally
            {
                Type1.GetMethod("BOConnect").Invoke(obj, null);
            }
            return dt;
        }
    }
}
